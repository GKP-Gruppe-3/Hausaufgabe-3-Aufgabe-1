# Hausaufgabe 3

## Aufgabe 1 - Quersumme & -produkt (11 Punkte)

### Aufgabe 1.1 - Programmverständnis (5 Punkte)

Versuchen Sie zunächst, den im Programm implementierten
Algorithmus nachzuvollziehen. Ergänzen Sie dazu für jede Programmanweisung
einen Kommentar, der deren Zweck und Wirkung beschreibt.

### Aufgabe 1.2 - Querprodukt (2 Punkte)

Erweitern Sie das Programm anschließend derart, dass neben der Quersumme auch das
Querprodukt für die vom Bildschirm eingelesene ganze Zahl n berechnet und ausgegeben
wird.

### Aufgabe 1.3 - Quersumme + Querprodukt = Zahl (4 Punkte)

Entwickeln Sie ausgehend von Ihrer Lösung aus Teil II) nun ein C-Programm, das alle
Zahlen zwischen 1 und 99 am Bildschirm ausgibt, für die gilt, dass die Summe von
Quersumme und Querprodukt wieder die Zahl selbst ergibt (beispielsweise 39, da (3 
9) + (3 + 9) = 39).
