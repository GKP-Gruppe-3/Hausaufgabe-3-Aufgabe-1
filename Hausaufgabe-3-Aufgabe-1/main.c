#include "stdafx.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

void pause()
{
  _getch();
  system("cls");
}

// Gets an int value from console.
double consoleGetInt(char *title)
{
  int i;
  printf(title);
  scanf_s("%d", &i);
  return i;
}

void computeDigitSumAndProduct(int n, int *digitSum, int *digitProduct)
{
  // Die Variable "digit" wird als 32-Bit-Ganzzahl deklariert.
  int digit;
  // Der Inhalt der Variable "digitSum" wird auf die Zahl Null gesetzt.
  *digitSum = 0;
  // Der Inhalt der Variable "digitProduct" wird auf die Zahl Eins gesetzt.
  *digitProduct = 1;

  // Die Variable "i" wird als 32-Bit-Ganzzahl deklariert.
  // Eine Zählschleife für "i" wird durchlaufen, solange "i" größer der Zahl Null ist.
  // Die Schleife beginnt mit der Zahl in "n".
  // Ab dem zweiten Schleifendurchlauf wird "i" auf das abgerundete Zehntel des vorigen Wertes gesetzt.
  for (int i = n; i > 0; i = i / 10)
  {
    // Die Variable "digit" (Ziffer) wird als Rest aus der Division von "i" durch 10 festgelegt.
    digit = (i % 10);

    // Zur Variable "digitSum" (Quersumme) wird "digit" aufaddiert.
    *digitSum += digit;
    // Zur Variable "digitProduct" (Querprodukt) wird "digit" aufmultipliziert.
    *digitProduct *= digit;
  }
}

// Lösung zu Unterpunkt 1.3
void testDigitSumAndProduct(void)
{
  int digitSum;
  int digitProduct;

  printf("Die Zahlen von 1 bis einschliesslich 99 werden durchlaufen.\n");
  printf("Alle dazu gehoerenden Zahlen werden ausgegeben, \n");
  printf("fuer die Quersumme und Querprodukt aufaddiert wieder die Ausgangszahl ergeben: \n");

  for (int n = 1; n <= 99; n++)
  {
    computeDigitSumAndProduct(n, &digitSum, &digitProduct);

    // Jede Zahl "n" ausgeben, deren Quersumme und Querprodukt aufaddiert "n" ergeben.
    if (digitSum + digitProduct == n)
    {
      printf("* Zahl = %d; Quersumme = %d; Querprodukt = %d\n", n, digitSum, digitProduct);
    }
  }
}

// Das Original-Hauptprogramm aus der Aufgabenbeschreibung erweitert
// um die Unterpunkte 1.1 und 1.2.
int mainOriginal(void)
{
  // Die Variablen "n", "i", "q" und "qProduct" werden als 32-Bit-Ganzzahlen deklariert.
  int n, i, q, qProduct;
  // Eine Mitteilung mit Beschreibung für den direkt folgenden Verlauf wird ausgegeben.
  printf("Geben Sie eine ganze Zahl ein: ");
  // Eine Ganzahl wird als Benutzereingabe in die Variable "n" eingelesen.
  scanf_s("%d", &n);

  // Die Variable "q" wird auf die Zahl Null gesetzt.
  q = 0;
  // Die Variable "qProduct" wird auf die Zahl Eins gesetzt.
  qProduct = 1;
  // Eine Zählschleife für "i" wird durchlaufen, solange "i" größer der Zahl Null ist.
  // Die Schleife beginnt mit der Zahl in "n".
  // Ab dem zweiten Schleifendurchlauf wird "i" auf das abgerundete Zehntel des vorigen Wertes gesetzt.
  for (i = n; i > 0; i = i / 10)
  {
    // Die Variable "q" wird um den Rest der Division von "i" durch 10 erhöht.
    q = q + (i % 10);
    // Die Variable "qProduct" wird aufmultipliziert mit dem Rest der Division von "i" durch 10.
    qProduct *= (i % 10);
  }

  // Die Variablen "n" und "q" werden mit Kommentar zu deren Bedeutung ausgegeben.
  printf("Die Quersumme von %d ist:   %d\n", n, q);
  // Die Variablen "n" und "qProduct" werden mit Kommentar zu deren Bedeutung ausgegeben.
  printf("Die Querprodukt von %d ist: %d\n", n, qProduct);

  // Error-Code ans Betriebssystem liefern.
  return 0;
}

int main(void)
{
  //*
  mainOriginal();
  pause();
  //*/

  //*
  testDigitSumAndProduct();
  pause();
  //*/

  // Error-Code ans Betriebssystem liefern.
  return 0;
}
